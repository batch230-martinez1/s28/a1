db.hotelRoom.insertOne({
  name: "single",
  accomodates: 2,
  price: 1000,
  description: "A simple room with all the basics necessities",
  room_available: 10,
  isAvailable: false
})

// insertMany()
db.hotelRoom.insertMany([
  {
  name: "double",
  accomodates: 3,
  price: 2000,
  description: "A room fit for a small family going on a vacation",
  room_available: 5,
  isAvailable: false
  },
  {
    name: "queen",
    accomodates: 4,
    price: 4000,
    description: "A room with a queen sized bed perfect for a simple getaway",
    room_available: 15,
    isAvailable: false
  }
 ]
)
// 5 use find
db.hotelRoom.find(
  {name : "double"}
)

// 6 use update
db.hotelRoom.updateOne(
  {name: "queen"},
  {
  $set: {
  room_available :0
  }
}
)

// 7 use deleteMany
db.hotelRoom.deleteMany(
  {
  room_available: 0
}
)







